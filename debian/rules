#!/usr/bin/make -f
# -*- makefile -*-

include /usr/share/python/python.mk

export NO_PKG_MANGLE=1
export PYTHONWARNINGS=d
export PYTHONHASHSEED=random
export http_proxy=http://127.0.0.1:9/

here = $(dir $(firstword $(MAKEFILE_LIST)))/..
debian_version = $(word 2,$(shell cd $(here) && dpkg-parsechangelog | grep ^Version:))
upstream_dfsg_version = $(firstword $(subst -, ,$(debian_version)))
upstream_version = $(subst ~,,$(firstword $(subst +, ,$(debian_version))))

locales = $(notdir $(patsubst %/LC_MESSAGES,%,$(wildcard sphinx/locale/*/LC_MESSAGES)))
scripts = $(basename $(wildcard sphinx-*.py))

debroot = debian/tmp/
site_packages = $(call py_libdir,$(shell pyversions -d))
py3_site_packages = $(call py_libdir,$(shell py3versions -d))
javascript_path = /usr/share/javascript/sphinxdoc/1.0/

python_all = pyversions -r | tr ' ' '\n' | xargs -t -I {} env {}
python3_all = py3versions -r | tr ' ' '\n' | xargs -t -I {} env {}

.PHONY: clean
clean:
	dh_clean 
	find . -name '*.py[co]' -delete
	rm -rf build
	rm -f $(basename $(wildcard debian/*.in))
	rm -f sphinx/pycode/Grammar*.pickle

.PHONY: build build-arch build-indep
build build-indep: build-stamp

sphinx/themes/basic/static/%.js:
	ln -sf /usr/share/javascript/$(*)/$(*).js $(@)
       
build/javascript-stamp: $(addprefix sphinx/themes/basic/static/,jquery.js underscore.js)
	echo $(^) | xargs -n1 | xargs -t -I {} [ -L {} ]
	mkdir -p $(dir $@)
	touch $(@)

build-stamp: sphinx-build.py build/javascript-stamp
	python ./sphinx-build.py -T doc build/html/
	find build/html/ -name '*.txt' -or -name '*.html' | xargs -L1 sed -i \
		's!http://docutils.sourceforge.net/docs/!file:///usr/share/doc/docutils-doc/docs/!g'
	rm -rf build/man
	cp -rl build/html build/man
	python ./sphinx-build.py -T -b man doc build/man
	python setup.py build --build-lib build/py2/
	python3 setup.py build --build-lib build/py3/
	python setup.py compile_catalog
ifeq "$(filter nocheck,$(DEB_BUILD_OPTIONS))" ""
	# FIXME: https://bitbucket.org/birkenfeld/sphinx/issue/703
	export LC_ALL=C.UTF-8 && \
	$(python_all) tests/run.py --verbose --no-skip
	export LC_ALL=C.UTF-8 && \
	$(python3_all) tests/run.py --verbose
	rm -rf build/py2/tests/ build/py3/tests/
	xvfb-run -a ./debian/jstest/run-tests build/html/
endif
	# import sphinx.pycode to generate grammar pickle files
	cd build/py2/ && python -c "import sphinx.pycode"
	cd build/py3/ && python3 -c "import sphinx.pycode"
	touch build-stamp

.PHONY: binary binary-arch binary-indep
binary binary-indep: build-stamp
	dh_testroot
	dh_installdirs
	python setup.py \
		build --build-lib build/py2/ \
		install --no-compile --install-layout=deb --root $(debroot)
	python3 setup.py \
		build --build-lib build/py3/ \
		install --no-compile --install-layout=deb --root $(debroot)
	rm $(debroot)/usr/lib/python*/*-packages/Sphinx-*.egg-info/SOURCES.txt
	# move static files outside {site,dist}-packages
	sed -i -e "s!^package_dir = .*!package_dir = '/usr/share/sphinx'!" \
		$(debroot)$(site_packages)/sphinx/__init__.py \
		$(debroot)$(py3_site_packages)/sphinx/__init__.py
	rm -rf $(debroot)/usr/share/sphinx/
	mkdir -p $(debroot)/usr/share/sphinx/
	cd $(debroot) && mv -t usr/share/sphinx/ \
		.$(site_packages)/sphinx/texinputs \
		.$(site_packages)/sphinx/themes
	mkdir -p $(debroot)/usr/share/sphinx/pycode/
	cd $(debroot) && mv -t usr/share/sphinx/pycode/ \
		.$(site_packages)/sphinx/pycode/Grammar*
	cd $(debroot) && mv -t usr/share/sphinx/pycode/ \
		.$(py3_site_packages)/sphinx/pycode/Grammar*
	mkdir -p $(debroot)/usr/share/sphinx/ext/autosummary/
	cd $(debroot) && mv -t usr/share/sphinx/ext/autosummary/ \
		.$(site_packages)/sphinx/ext/autosummary/templates
	cd $(debroot)$(py3_site_packages)/sphinx/ && \
		rm -rf texinputs/ themes/ ext/autosummary/templates/
	set -e -x; \
	cd $(debroot) && \
		for lang in $(locales); \
		do \
			install -m 644 -D .$(site_packages)/sphinx/locale/$$lang/LC_MESSAGES/sphinx.js \
				usr/share/sphinx/locale/$$lang/sphinx.js; \
			install -m 644 -D .$(site_packages)/sphinx/locale/$$lang/LC_MESSAGES/sphinx.mo \
				usr/share/locale/$$lang/LC_MESSAGES/sphinx.mo; \
		done
	cd $(debroot)$(site_packages)/sphinx/locale && rm -rf sphinx.pot */
	cd $(debroot)$(py3_site_packages)/sphinx/locale && rm -rf sphinx.pot */
	# install scripts not touched by easy install
	set -e -x; \
	for python in python2 python3; do \
		dir=$(debroot)/usr/share/sphinx/scripts/$$python/; \
		mkdir -p $$dir; \
		for script in $(scripts); do \
			sed -e "1 s,#!.*,#!/usr/bin/$${python%2}," $$script.py \
			> $$dir/$$script; \
			rm -f $(debroot)/usr/bin/$$script; \
		done; \
		chmod 755 $$dir/*; \
	done
	set -e -x; \
	for maintscript in $(wildcard debian/sphinx-common.*.in); do \
		sed -e 's/@SCRIPTS@/$(scripts)/' $$maintscript > $${maintscript%.in}; \
	done
	# Move JavaScript code to libjs-sphinxdoc:
	debian/dh-sphinxdoc/install-js debian/libjs-sphinxdoc/$(javascript_path)/
	set -e; \
	for js in $$(find $(debroot)/usr/share/sphinx/themes/ -name '*.js' -a '!' -name 'websupport.js'); do \
		mv $$js debian/libjs-sphinxdoc/$(javascript_path); \
		ln -sf "$(javascript_path)/$${js##*/}" $$js; \
	done
	dh_install -p sphinx-common -X /scripts/
	dh_install -N sphinx-common --fail-missing
	dh_installchangelogs CHANGES
	dh_installdocs
	dh_lintian
	./debian/dh-sphinxdoc/dh_sphinxdoc -p sphinx-doc /usr/share/doc/sphinx-doc/html/
	dh_installexamples
	cd debian/dh-sphinxdoc/ && pod2man -c Debhelper -r '' dh_sphinxdoc dh_sphinxdoc.1
ifneq ($(shell grep -h '^[.]TH' debian/*.1 | cut -d ' ' -f 6-7 | sort -u), "Sphinx $(upstream_version)")
	$(warning W: version numbers in the manual pages are out of date)
endif
	dh_installman
	dh_python2 -p python-sphinx
	dh_python3 -p python3-sphinx --no-guessing-deps
	dh_compress -X.py -X.rst -X.json -X.txt
	dh_link
	dh_fixperms
	dh_installdeb
	dh_gencontrol
	dh_md5sums
	dh_builddeb

.PHONY: get-orig-source
get-orig-source:
	sh $(here)/debian/get-orig-source.sh $(upstream_dfsg_version)

# vim:ts=4 sw=4 noet
